package formWeb.prodemy;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Form
 */
@WebServlet("/Form")
public class Form extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	
		String nama = request.getParameter("nama");
		String nim = request.getParameter("nim");
		String tanggalLahir = request.getParameter("tanggal_lahir");
		String alamat = request.getParameter("alamat");
		String fakultas = request.getParameter("fakultas");
		String prodi = request.getParameter("prodi");
		
		PrintWriter out = response.getWriter();

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		String nama = request.getParameter("nama");
		String nim = request.getParameter("nim");
		String tanggalLahir = request.getParameter("tanggal_lahir");
		String alamat = request.getParameter("alamat");
		String fakultas = request.getParameter("fakultas");
		String prodi = request.getParameter("prodi");
		
		try {
			
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		String DB_URL = "jdbc:postgresql://localhost/employee";
		String USER = "postgres";
		String PASS = "31052000";
		
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			Statement stmt = conn.createStatement();
			
			String inputDataSql = "insert into bio(nama, nim, tanggal_lahir, alamat, fakultas, prodi)" + "values" + "('" + nama + "','" + nim + "','" + tanggalLahir + "','" + alamat + "','" + fakultas + "','" + prodi + "')";
			System.out.println(inputDataSql);
			stmt.execute(inputDataSql);
			System.out.println("inserted record into table");
			
			request.setAttribute("titipan1", nama);
			
			request.getRequestDispatcher("view-biodata.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
					conn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

}
